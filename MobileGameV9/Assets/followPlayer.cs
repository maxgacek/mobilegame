﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPlayer : MonoBehaviour {

    public Transform player;

    Vector3 campos;

    public float CameraSpeed = 0.25f;



	void Update () {
        campos.x = Mathf.Lerp(transform.position.x, player.position.x, CameraSpeed);
        campos.y = Mathf.Lerp(transform.position.y, player.position.y, CameraSpeed);
        campos.z = -10f;

        transform.position = campos;
    }
}
