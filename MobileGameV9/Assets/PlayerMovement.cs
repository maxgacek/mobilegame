﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    Vector3 pos;                             
    public float speed = 1.0f;     
    public float lerpSpeed = 1.0f;
    onHit onhit;

    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
  
        pos = transform.position;         
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.A) && transform.position == pos)
        {
            pos += Vector3.left * speed;
            playAnimation("walk");

        }
        if (Input.GetKey(KeyCode.D) && transform.position == pos)
        {
            pos += Vector3.right * speed;
            playAnimation("walk");

        }
        if (Input.GetKey(KeyCode.W) && transform.position == pos)
        {
            pos += Vector3.up * speed;
            playAnimation("walk");

        }
        if (Input.GetKey(KeyCode.S) && transform.position == pos)
        {
            pos += Vector3.down * speed;
            playAnimation("walk");

        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            playAnimation("mine");
        }

        transform.position = Vector3.LerpUnclamped(transform.position, pos, lerpSpeed); 
    }


    public void playAnimation(string type)
    {
        if (type == "walk")
            animator.SetTrigger("playerMove");

        if (type == "mine")
            animator.SetTrigger("Mine");
    }
}
