﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onHit : MonoBehaviour {

    public PlayerMovement player;

    public int health = 3;

	// Use this for initialization
	void Start () {
        //player = GetComponent<PlayerMovement>();
	}
	
	void Update () {
		
        if(health <= 0)
        {
            Destroy(gameObject);
        }

	}


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            health--;
            StartCoroutine(Die());
        }
    }

    void play()
    {
        player.playAnimation("mine");
    }

    IEnumerator Die()
    {
        play();
        yield return new WaitForSeconds(1);
        health--;

    }
}
